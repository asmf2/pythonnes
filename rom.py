import sys
import struct

HEADER_SIZE = 16
KB_SIZE = 16384

class ROM (object):
	def __init__ (self, rom_bytes): 
		self.header = rom_bytes [0:HEADER_SIZE]
		self.num_prg_blocks = (self.header [4])
		if (sys.version_info < (3,)):
			self.num_prg_blocks = ord (self.header [4])
		print (type (self.num_prg_blocks))
		self.data_bytes = rom_bytes [HEADER_SIZE:HEADER_SIZE + (HEADER_SIZE + KB_SIZE * self.num_prg_blocks)]

	def get_byte (self, pc):
		return	(self.data_bytes [pc])

