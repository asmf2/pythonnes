import sys
from abc import ABCMeta, abstractmethod, abstractproperty

class Instruction (object):
	__metaclass_ = ABCMeta
		
	def __init__ (self,identification_byte):
		self.identity_byte = identification_byte

	@abstractproperty
	def instruction_length (self):
		pass

	@abstractmethod
	def process (self,registers,ram,rom):
		pass

	def set_bit_2_SR (self,registers):
		print ("registers.status before setting bit 2: ", registers.status)
		set_bit_2 = 2
		registers.status = registers.status | set_bit_2
		print ("registers.status after setting bit 2: ", registers.status)

	def set_bit_7_SR (self,registers,register):
		all_bits_set_except7 = 127
		#print ("Type of register: ", type (register))
		all_bits_set_except7 = all_bits_set_except7 | register
		if (all_bits_set_except7 == 255):
			print ("registers.status before setting bit 7: ", registers.status)
			set_bit_7 = 128
			registers.status = registers.status | set_bit_7
			print ("registers.status after setting bit 7: ", registers.status)

	def absolute_address_calculation (self,registers,rom):
		address1 = rom.data_bytes [registers.pc+1]
		address2 = rom.data_bytes [registers.pc+2]
		print ("address1: ", address1, "address2: ", address2)
		address_final = address2 << 8 + address1
		print ("address_final: ", address_final)


class ADCInstruction (Instruction):
	def process (self,registers,ram,rom):
		if (registers.accumulator==0):
			self.set_bit_2_SR (registers)
		print ("accumulator before: ", registers.accumulator)
		add (self,registers,ram,rom)
		check_overflow (registers)
		print ("accumulator before: ", registers.accumulator)

	def check_overflow (registers):
		if (registers.acumulator>255):
			set_bit_1_carry = 1
			registers.status = registers.status | set_bit_1_carry

class ADCImmediate (ADCInstruction):
	def add (self,registers,ram,rom):
		registers.acumulator += rom.data_bytes [registers.pc+1]
	instruction_length=2

class ADCZeroPage (ADCInstruction):
	def add (self,registers,ram,rom):	
		registers.acumulator += rom.data_bytes [rom.data_bytes [registers.pc+1]]
	instruction_length=2

class ADCZeroPageX (ADCInstruction):
	def add (self,registers,ram,rom):	
		address = registers.index_x + rom.data_bytes [rom.data_bytes [registers.pc+1]]
		#if (address>255)
		registers.acumulator += address
	instruction_length=2 



class BPLInstruction (Instruction):
	def process (self,registers,ram,rom):
		print ("Identifier Byte: BPLInstruction:",hex (self.identity_byte), self.identity_byte)
		_128 = 128
		_128 = _128 & registers.status
		print ("registers.status: ", registers.status)
		print ("_128", _128)
		if (_128 == 0):		
			if (rom.data_bytes[registers.pc+1] == 251):
				print ("FB")
				registers.pc += -3  
			elif (rom.data_bytes[registers.pc+1] == 252):
				print ("FC")
				registers.pc += -2
			elif (rom.data_bytes[registers.pc+1] == 253):
				print ("FD")
				registers.pc += -1
			elif (rom.data_bytes[registers.pc+1] == 254):
				print ("FE")
				registers.pc += 0
			elif (rom.data_bytes[registers.pc+1] == 255):
				print ("FF")
				registers.pc += 1
			elif (rom.data_bytes[registers.pc+1] == 1):
				print ("+1")
				registers.pc += 3														
			elif (rom.data_bytes[registers.pc+1] == 1):
				print ("+1")
				registers.pc += 3
		print ("registers.pc: ", registers.pc)
	instruction_length=0
		
class CLDInstruction (Instruction):
	def process (self,registers,ram,rom):
		print ("Identifier Byte: CLDInstruction:",hex (self.identity_byte), self.identity_byte)
#		temp=247
#		registers.status = registers.status & temp 
		print ("CLDInstruction: does nothing on the NES: ")
	instruction_length = 1

class CLIInstruction (Instruction):
	def process (self,registers,ram,rom):
		print ("Identifier Byte: CLIInstruction:",hex (self.identity_byte), self.identity_byte)
		print ("Status registers before a CLI instruction: ", registers.status)
		temp = 251
		registers.status = registers.status & temp
		print ("Status registers after a CLI instruction: ", registers.status)
	instruction_length = 1
		
class LDA (Instruction):
	def process (self,registers,ram,rom):
		print ("Identifier Byte: LDAInstruction:",hex (self.identity_byte), self.identity_byte)		
		if (registers.accumulator==0):
			self.set_bit_2_SR (registers)
		print ("registers.accumulator: ", registers.accumulator)
		self.set_bit_7_SR (registers,registers.accumulator)
		self.writeAccumulator (registers,ram,rom)
	
	@abstractproperty
	def instruction_length (self):
		return 1

	@abstractmethod
	def writeAccumulator (self,registers,ram,rom):
		pass

class LDAImediate (LDA):
	def writeAccumulator (self,registers,ram,rom):
		registers.accumulator = rom.data_bytes[registers.pc+1]
		print ("registers.accumulator: ", registers.accumulator)
	instruction_length = 2

class LDAAbsolute (LDA):
	def writeAccumulator (self,registers,ram,rom):
		address = absolute_adress_calculation (registers,rom)
		registers.accumulator = rom.data_bytes [address]
		print ("registers.accumulator: ", registers.accumulator)
	instruction_length = 3

class LDAZero (LDA):
	def writeAccumulator (self,registers,ram,rom):
		registers.accumulator = rom.data_bytes[address2]


class LDX (Instruction):
	def process (self,registers,ram,rom):
		print ("Identifier Byte: LDXInstruction :",hex (self.identity_byte), self.identity_byte)		
		if (registers.index_x==0):
			self.set_bit_2_SR (registers)
		print ("registers.index_x before: ", registers.index_x)
		self.set_bit_7_SR (registers,registers.index_x)
		self.writeX (registers,ram,rom)
		print ("registers.index_x after: ", registers.index_x)

class LDXImmediate (LDX):
	def writeX (self,registers,ram,rom):
		registers.index_x = rom.data_bytes[registers.pc+1]
	instruction_length = 2

class LDXAbsolute (LDX):
	def writeX (self,registers,ram,rom):
		address = absolute_address_calculation (registers,rom)
		registers.index_x = rom.data_bytes[address]
	instruction_length = 3



class LDY (Instruction):
	def process (self,registers,ram,rom):
		print ("Identifier Byte: LDYInstruction :",hex (self.identity_byte), self.identity_byte)
		if (registers.index_y==0):		
			self.set_bit_2_SR (registers)	
		print ("registers.index_y before: ", registers.index_y)
		self.set_bit_7_SR (registers,registers.index_y)
		self.writeY (registers,ram,rom)
		print ("registers.index_y after: ", registers.index_y)

class LDYImmediate (LDY):
	def writeY (self,registers,ram,rom):
		registers.index_y = rom.data_bytes [registers.pc+1]
	instruction_length = 2

class LDYZero (LDY):
	def writeY (self,registers,ram,rom):
		registers.accumulator = rom.data_bytes [rom.data_bytes[register.pc+1]] 
	instruction_length=2

class LDYZeroX (LDY):
	def writeY (self,registers,ram,rom):
		address = registers.index_x + rom.data_bytes[register.pc+1]
		registers.accumulator =  rom.data_bytes [address] 
	instruction_length=2

class LDYAbsolute (LDY):
	def writeY (self,registers,ram,rom):
		address = absolute_address_calculation (registers,rom)
		registers.index_y = rom.data_bytes[address]
	instruction_length = 3

class LDYAbsoluteX (LDY):
	def writeY (self,registers,ram,rom):
		address = absolute_address_calculation (registers,rom)
		registers.index_y = rom.data_bytes[address + registers.index_x]
	instruction_length = 3


class SEIInstruction (Instruction):
	def process (self,registers,ram,rom):
		print ("Identifier Byte: SEIInstruction:",hex (self.identity_byte), self.identity_byte)
		temp = 4
		print ("registers.status: ", registers.status)
		registers.status = registers.status | temp
		print ("registers.status: ", registers.status)
	instruction_length = 1

class STAAccumulatorAbs (Instruction):
	def process (self,registers,ram,rom):
		print ("Identifier Byte: STAabsInstruction:",hex (self.identity_byte), self.identity_byte)
		address = absolute_address_calculation (registers,rom)
		ram.two_KB_internal_ram [address] = registers.accumulator
		print ("ram [", address, "]after accumulator: ", ram.two_KB_internal_ram [address])
	instruction_length = 3

class TSX_index (Instruction):
	def process (self,registers,ram,rom):
		print ("Identifier Byte: TSX_index:",hex (self.identity_byte), self.identity_byte)	
		print ("Stack_pointer current value: ", registers.stack_pointer)
		print ("Index_x before value: ", registers.index_x)
		if (registers.index_x==0):
			self.set_bit_2_SR (registers,ram,rom)
		self.set_bit_7_SR (registers,register)
		registers.index_x = registers.stack_pointer
		print ("registers.index_x: ", registers.index_x)
	instruction_length = 1

class TXS_index (Instruction):
	def process (self,registers,ram,rom):
		print ("Identifier Byte: TXS_index:",hex (self.identity_byte), self.identity_byte)	
		print ("Stack_pointer before value: ", registers.stack_pointer)
		print ("Index_x before value: ", registers.index_x)
		registers.stack_pointer = registers.index_x
		print ("Stack_pointer after value: ", registers.stack_pointer)
	instruction_length = 1	
