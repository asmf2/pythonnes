import sys

from ram import RAM 
from rom import ROM
from registers import Registers 
from instruction import ADCImmediate, ADCZeroPage, ADCZeroPageX, BPLInstruction, CLDInstruction, CLIInstruction, LDAImediate, LDAAbsolute, LDXImmediate, LDYImmediate, LDYZero, LDYZeroX, LDYAbsolute, LDYAbsoluteX, SEIInstruction, STAAccumulatorAbs, TSX_index, TXS_index

class CPU (object):
	def __init__(self, rom_bytes):
		self.rom = ROM (rom_bytes)
		self.ram = RAM ()
		self.registers = Registers ()
	
	def identify_instruction (self, byte):
		if byte==0x69:
			instruction = ADCImmediate (byte)
		elif byte==0x65:
			instruction = ADCZeroPage (byte)
		elif byte==0x75:
			instruction = ADCZeroPageX (byte)		
		elif byte==0x10:
			instruction = BPLInstruction (byte)
		elif byte==0xD8:
			instruction = CLDInstruction (byte)
		elif byte==0xA9: 
			instruction = LDAImediate (byte)
		elif byte==0xAD: 
			instruction = LDAAbsolute (byte)
		elif byte==0xA2:
			instruction = LDXImmediate (byte)
		elif byte==0xA0:
			instruction = LDYImmediate (byte)
		elif byte==0xA4:
			instruction = LDYZero (byte)
		elif byte==0xB4:
			instruction = LDYZeroX (byte)
		elif byte==0xAC:
			instruction = LDYAbsolute (byte)
		elif byte==0xBC:
			instruction = LDYAbsoluteX (byte)
		elif byte==0x78:	
			instruction = SEIInstruction (byte)
		elif byte==0x8D:
			instruction = STAAccumulatorAbs (byte)
		elif byte==0x9A:
			instruction = TXS_index (byte)
		else:
			instruction = None
		return instruction

	def process_instructions (self):
		for byte in self.rom.data_bytes:
			byte = self.rom.get_byte (self.registers.pc)
			print ("Byte: ",byte)
			print ("self.pc, antes: ", self.registers.pc)
			instruction = self.identify_instruction (byte)
			if (instruction == BPLInstruction):			
				break
				print ("LOMBO")
			if instruction is None:
				raise Exception ("Instruction not found")
			instruction.process (self.registers,self.ram,self.rom)
			self.registers.pc+= instruction.instruction_length
			print ("self.pc, depois: ", self.registers.pc)
			print ("")
			if self.registers.pc>=19:
				break
			
