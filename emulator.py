import argparse
from random import randint

from cpu import CPU

def main ():

	parser = argparse.ArgumentParser (description='NES EMULATOR'); 
	parser.add_argument ('rom_path',metavar='R',type=str,help='path to the rom')
	args=parser.parse_args()

	with open (args.rom_path, 'rb') as file:
		rom_bytes = file.read ()				 

	cpu = CPU(rom_bytes)
	cpu.process_instructions ()

if __name__ == '__main__':
	main () 
